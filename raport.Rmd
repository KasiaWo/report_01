---
title: "Liquidity Report"
date: '`r strftime(Sys.time(), format = "%B %d, %Y")`'
output:
  html_document:
    number_sections: yes
    theme: united
    toc: yes
    toc_depth: 2
params:
  directory:
    value: "C:/Users/Kasia1/Magisterka/Praca_mag"
  file:
    value: "macierz_cosinus.csv"
  variables_list:
    value: NULL
  numeric_factors: NULL
  date_columns: NULL
  target_column: 2
  predicted_column: 1
  old_model_column: 2
---

```{r, include = T}
knitr::opts_chunk$set(echo = F,
                      warning = F,
                      message = F)

source("C:/Users/Kasia1/Moje_rozne/R/functions_to_report.R")
## Set options
options(scipen = 999, # prevent scientific notation on large numbers
        stringsAsFactors = TRUE) 


 


## load dataset
dataset <- read.csv(paste(params$directory, params$file, sep = '/'))
head(dataset)
print(is.null(params[["numeric_factors"]]))

# this column can't be NA
 target_column <- get_column(dataset, column_index = params[["target_column"]], column_name = "target")
 predicted_column <- get_column(dataset, column_index = params[["predicted_column"]], column_name = "new predict")
 old_model_column <- get_column(dataset, column_index = params[["old_model_column"]], column_name = "old model")

 date_columns <- get_column_date_factor(dataset, params[["date_columns"]], column_name = "date")
 factor_columns <- get_column_date_factor(dataset, params[["numeric_factors"]], column_name = "factor")

 variables_columns <- get_variables(dataset, params[["variables_list"]], target_column, predicted_column,
                                    old_model_column, date_columns)
print( is.null(date_columns))
print( factor_columns)
print(variables_columns)


if(!is.null(factor_columns)){
  dataset[factor_columns] <- lapply(factor_columns, function(x) factor(dataset[[x]]))
}



for( variable in variable_columns){
  # pltlo
}

```